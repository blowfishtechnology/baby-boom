<?
include($_SERVER['DOCUMENT_ROOT'].'/octopus/includes/config.php');
//Include database connection detail
include($_SERVER['DOCUMENT_ROOT'].'/octopus/includes/functions.php');
// Connect to database
$db = new Database;

$db->query("select * from octo_custom2 where status = 'published'");
$data = $db->resultset();


$db->query("select * from octo_attachments where pid_type = 'custom3' and pid = '2'");
$slider = $db->resultset();
?>
<!DOCTYPE html>
<!-- HTML5 Mobile Boilerplate -->
<!--[if IEMobile 7]><html class="no-js iem7"><![endif]-->
<!--[if (gt IEMobile 7)|!(IEMobile)]><!-->
<html class="no-js" lang="en"><!--<![endif]-->

<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

<head>
	<meta charset="utf-8">
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Baby Boom Photography</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta http-equiv="cleartype" content="on">
	<link rel="shortcut icon" href="/favicon.ico">

	<meta property="og:url" content="http://stingrayspamfilter.com/dev/bbp/index.php"/>
	<meta property="og:type" content="website"/>
	<meta property="og:title" content="Baby Boom Photography"/>
	<meta property="og:description" content="Thank you for visiting my site. How lucky am I to have a job that I love.  I love to capture a magic moment and freeze it in time. Creating a memory that you can keep forever."/>
	<meta property="og:image" content="http://stingrayspamfilter.com/dev/bbp/imgs/og_logo.png"/>

	<!-- Responsive and mobile friendly stuff -->
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/html5reset.css" media="all">
	<link rel="stylesheet" href="css/responsivegridsystem.css" media="all">
  <link rel="stylesheet" href="css/main.css" media="all">
	<link rel="stylesheet" href="css/col.css" media="all">
	<link rel="stylesheet" href="css/2cols.css" media="all">
	<link rel="stylesheet" href="css/3cols.css" media="all">
	<link rel="stylesheet" href="css/4cols.css" media="all">
	<link rel="stylesheet" href="css/5cols.css" media="all">
	<link rel="stylesheet" href="css/6cols.css" media="all">
	<link rel="stylesheet" href="css/7cols.css" media="all">
	<link rel="stylesheet" href="css/8cols.css" media="all">
	<link rel="stylesheet" href="css/9cols.css" media="all">
	<link rel="stylesheet" href="css/10cols.css" media="all">
	<link rel="stylesheet" href="css/11cols.css" media="all">
	<link rel="stylesheet" href="css/12cols.css" media="all">
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="icon"
			type="image/png"
			href="imgs/Logo.png">
	<!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements and feature detects -->
	<script src="js/modernizr-2.5.3-min.js"></script>
	<style type="text/css">
/*
Recommeded CSS
==============
*/
.sessP{
	margin-bottom: 10px;
}

.sessH{
	margin-bottom: 10px;
	margin-top: 20px;
		font-family: 'Fredericka the Great', cursive;
		font-size: 19px;
}
</style>
</head>
<body>
 <nav id="mobmenu" class="panel" role="navigation">
      <ul class="mobList">
          <li><a href="index.php">Home</a></li>
          <li><a href="gallery.php">Gallery</a></li>
          <li><a href="prices.php">Prices</a></li>
          <li><a href="cake_smash.php">Cake Smash</a></li>
          <li><a href="newborn_session.php">Newborn Session</a></li>
          <li><a href="blog.php">Blog</a></li>
          <li><a href="contact_us.php">Contact</a></li>
      </ul>
         <a href="#mobmenu" class="sldr-menu-link">Close <i class="fa fa-bars"></i></a>
  </nav>
  <div class="wrap push">
 <div id="rwd_indicator"> <!--TopBar-->
  <div id="topRepeat"></div>
 </div>
 <div class="logo">
  <div class="col span_3_of_3" id="logoBar">
   <!-- This is where the image will go-->
   <a href="index.php"><img src="imgs/Logo.png" alt="Logo"></a>
  </div>
 </div>
 <div id="menu">
 <div id="mobTop">
    <a href="#mobmenu" class="menu-link">Menu <i class="fa fa-bars"></i></a>
    <div id="call">

      </div>
 </div>
   <ul class="mainNav">
       <li><a href="index.php">Home</a></li>
       <li><a href="gallery.php">Gallery</a></li>
       <li><a href="prices.php">Prices</a></li>
       <li><a href="cake_smash.php" style="width:148px;">Cake Smash</a></li>
<li><a href="newborn_session.php" style="">Newborn Session</a></li>
       <li><a href="blog.php">Blog</a></li>
       <li><a href="contact_us.php">Contact</a></li>
   </ul>
 </div>
		<div id="wrapper">
		    <div class="maincontent">

					<div class="section group">
						<div class="col span_3_of_3">
								<h2>Prices</h2>
						</div>
					</div>
					<div class="section group">
							<div class="col span_1_of_2">
								<center>
								<table>
								  <tr>
												<th>Title</th>
												<th>Details</th>
								    <th>Price</th>
								  </tr>
										<?foreach ($data as $d) {?>
											<tr>
														<td><?echo $d['name'];?></td>
														<td><?echo $d['details'];?></td>
														<td><?echo $d['varchar_1'];?></td>
											</tr>
										<?}?>
											<!-- <tr>
														<td>Newborn &amp; Family shots</td>
								    		<td>8 Digital Images of your choice on USB, duplicated in Black and White (With full copyright)</td>
														<td>£195</td>
							  		</tr>
											<tr>
														<td>Cakesmah Clean Up &amp; Teddy Hug</td>
														<td>15 Digital Images of your choice on USB, 10"x 8" digital montage of the photoshoot on USB (With full copyright) Cake not included (due to possible allergies)</td>
														<td>£145</td>
											</tr> -->
											<tr>
														<td colspan="3">Other products and prints are available upon request.</td>
											</tr>
								</table>
								<!--<img src=" http://stingrayspamfilter.com/octopus/uploads/Arthur george mcnally-montage.jpg"/>-->
								</center>
							</div>
						<div class="col span_1_of_2">
							<div class="clear-fix"></div>
       <p class="sessH" style="margin-top: 0px;">Prices:</p>
       <p class="sessP">
        1 - 3 hour studio time on all sessions.
        £20 deposit to secure the date.
       </p><br />
							<p class="sessH" style="margin-top: 0px;">Older baby &amp; toddler:</p>
							<p class="sessP">
								The session can also last up to 3 hours depending on the baby or toddler, so bring at least three changes of clothes,
								as I have different backdrops and props that I like to use. Please also bring some top-up milk for baby and any snacks that your toddler will like.
							</p>
						</div>
					</div>
				</div>
		</div>
		<div class="site-footer"></div>
		 <div class="postFooter">
			 <div id="bottomRepeat"></div>
			</div>
</div>
		<!-- JavaScript at the bottom for fast page loading -->

		<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/jquery-1.7.2.min.js"><\/script>')</script>

	    -->
			<script src="js/jq.js"></script>
			<script src="js/slide.js"></script>
			<script>
					$(document).ready(function() {
							$('.menu-link').bigSlide();
					});

					var myIndex = 0;
					carousel();

					var slideIndex = 1;
					showDivs(slideIndex);

					function plusDivs(n) {
						showDivs(slideIndex += n);
					}

					function showDivs(n) {
						var i;
						var x = document.getElementsByClassName("mySlides");
						if (n > x.length) {slideIndex = 1}
						if (n < 1) {slideIndex = x.length}
						for (i = 0; i < x.length; i++) {
							 x[i].style.display = "none";
						}
						x[slideIndex-1].style.display = "block";
					}

					function carousel(){
						var i;
						var x = document.getElementsByClassName("mySlides");
						for (i = 0; i < x.length; i++) {
								x[i].style.display = "none";
						 }
						 myIndex++;
						 if (myIndex > x.length) {myIndex = 1}
						 x[myIndex-1].style.display = "block";
						 setTimeout(carousel, 10000); // Change image every 10 seconds
					}
			</script>
	    <script>
	        $(document).ready(function() {
	            $('.menu-link').bigSlide();
	        });
	    </script>
		<script src="js/backstretch.js"></script>
		<!--<script> $("#banner").backstretch("imgs/ormskirk.jpg");</script>-->
</body>
</html>
