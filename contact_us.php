<?
if(isset($_GET['blank'])){
	$blank = '1';
}else{
	$blank = '0';
}

if(isset($_GET['error'])){
	$error = '1';
}else{
	$error = '0';
}

?>
<!DOCTYPE html>
<!-- HTML5 Mobile Boilerplate -->
<!--[if IEMobile 7]><html class="no-js iem7"><![endif]-->
<!--[if (gt IEMobile 7)|!(IEMobile)]><!-->
<html class="no-js" lang="en"><!--<![endif]-->

<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

<head>
	<meta charset="utf-8">
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Baby Boom Photography</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta http-equiv="cleartype" content="on">
	<link rel="shortcut icon" href="/favicon.ico">

	<meta property="og:url" content="http://stingrayspamfilter.com/dev/bbp/index.php"/>
	<meta property="og:type" content="website"/>
	<meta property="og:title" content="Baby Boom Photography"/>
	<meta property="og:description" content="Thank you for visiting my site. How lucky am I to have a job that I love.  I love to capture a magic moment and freeze it in time. Creating a memory that you can keep forever."/>
	<meta property="og:image" content="http://stingrayspamfilter.com/dev/bbp/imgs/og_logo.png"/>

	<!-- Responsive and mobile friendly stuff -->
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/html5reset.css" media="all">
	<link rel="stylesheet" href="css/responsivegridsystem.css" media="all">
  <link rel="stylesheet" href="css/main.css" media="all">
	<link rel="stylesheet" href="css/col.css" media="all">
	<link rel="stylesheet" href="css/2cols.css" media="all">
	<link rel="stylesheet" href="css/3cols.css" media="all">
	<link rel="stylesheet" href="css/4cols.css" media="all">
	<link rel="stylesheet" href="css/5cols.css" media="all">
	<link rel="stylesheet" href="css/6cols.css" media="all">
	<link rel="stylesheet" href="css/7cols.css" media="all">
	<link rel="stylesheet" href="css/8cols.css" media="all">
	<link rel="stylesheet" href="css/9cols.css" media="all">
	<link rel="stylesheet" href="css/10cols.css" media="all">
	<link rel="stylesheet" href="css/11cols.css" media="all">
	<link rel="stylesheet" href="css/12cols.css" media="all">
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="icon" type="image/png" href="imgs/Logo.png">
	<!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements and feature detects -->
	<script src="js/modernizr-2.5.3-min.js"></script>
	<style type="text/css">
	.formHolder{
		margin-top: 15px;
		margin-bottom: 15px;
	}

	.input_form{

		border: 0px solid white;
		-webkit-box-shadow: 0 0px 0px #333;
		-moz-box-shadow: 0 0px 0px #333;
		box-shadow: 0 0px 0px #333;
		border-bottom: 2px solid #C0B3AB;
		border-radius: 0px;
	}
	.input_form:focus{
		outline: none;
	}

	.social_holder{
		height:65px;
		width:65px;
		background-position: center;
		background-repeat: no-repeat;
		float:left;
		padding: 10px;
	}

	.social{
		margin: 0 auto;
		display: table;
	}

	.submit{
		background-color: rgba(192,179,171,0.1);
		border: 0px solid white;
		-webkit-box-shadow: 0 0px 0px #333;
		-moz-box-shadow: 0 0px 0px #333;
		box-shadow: 0 0px 0px #333;
		border-bottom: 2px solid #C0B3AB;
		border-radius: 0px;
		width: 51%;
	}

	input, textarea{
		background-color: rgba(192,179,171,0.1);
		color:#65594A;
		font-family: 'Amatic SC', cursive;
	}

	.sent-message{
		z-index: 1000000;
		position: absolute;

		height: 170px;
		width: 400px;
		top: 50%;
		left: 50%;
		margin-left: -200px;
		-webkit-box-shadow: 10px 10px 37px -6px rgba(0,0,0,0.75);
		-moz-box-shadow: 10px 10px 37px -6px rgba(0,0,0,0.75);
		box-shadow: 10px 10px 37px -6px rgba(0,0,0,0.75);
		border-top: 2px solid #C0B3AB;
		border-bottom: 2px solid #C0B3AB;
	}

	.sent-message-inner{
		background-color: white;
		height: 100%;
		width: 100%;
	}

	.sent-text{
		font-family: 'Amatic SC', cursive;
		font-size: 18px;
	}

	.sent-close{
		cursor: pointer;
	}

	@media only screen and (max-width: 480px) {
		.sent-message{
			height: 190px;
			width: 300px;
			margin-left: -150px;
		}
	}
</style>
</head>
<body>
 <nav id="mobmenu" class="panel" role="navigation">
      <ul class="mobList">
          <li><a href="index.php">Home</a></li>
          <li><a href="gallery.php">Gallery</a></li>
          <li><a href="prices.php">Prices</a></li>
          <li><a href="cake_smash.php">Cake Smash</a></li>
          <li><a href="newborn_session.php">Newborn Session</a></li>
          <li><a href="blog.php">Blog</a></li>
          <li><a href="contact_us.php">Contact</a></li>
      </ul>
         <a href="#mobmenu" class="sldr-menu-link">Close <i class="fa fa-bars"></i></a>
  </nav>
  <div class="wrap push">
 <div id="rwd_indicator"> <!--TopBar-->
  <div id="topRepeat"></div>
 </div>
 <div class="logo">
  <div class="col span_3_of_3" id="logoBar">
   <!-- This is where the image will go-->
   <a href="index.php"><img src="imgs/Logo.png" alt="Logo"></a>
  </div>
 </div>
 <div id="menu">
 <div id="mobTop">
    <a href="#mobmenu" class="menu-link">Menu <i class="fa fa-bars"></i></a>
    <div id="call">

      </div>
 </div>
   <ul class="mainNav">
       <li><a href="index.php">Home</a></li>
       <li><a href="gallery.php">Gallery</a></li>
       <li><a href="prices.php">Prices</a></li>
       <li><a href="cake_smash.php" style="width:148px;">Cake Smash</a></li>
       <li><a href="newborn_session.php" style="">Newborn Session</a></li>
       <li><a href="blog.php">Blog</a></li>
       <li><a href="contact_us.php">Contact</a></li>
   </ul>
 </div>
			<?if($blank == 1 || $error == 1){?>
			<div class="sent-message">
				<div class="sent-message-inner">
					<?if($blank == 1){?>
						<p class="sent-text" style="padding-top: 10px;">
							Please make sure all the fields are filled out.
						</p>
						<p class="sent-text">
							Please try again
						</p>
					<?}else if($error == 1){?>
						<p class="sent-text" style="padding-top: 10px;">
							An error has occurred sending your message.
						</p>
						<p class="sent-text">
							Please try again later or call on 12345678901
						</p>
					<?}?>
					<i class="fa fa-times sent-close"></i>
				</div>
			</div>
			<?}?>
		<div id="wrapper">
		    <div class="maincontent">
					<div class="section group">

							<strong>Request Callback</strong>
							<center>
								<form name="email-form" id="email-form" method="POST" action="scripts/email-form.php">
									<div class="formHolder">
										<input class="input_form" id="appn" name="appn" type="text" placeholder="Name..." style="height: 25px; width: 50%;" />
									</div>
									<div class="formHolder">
										<input class="input_form" id="appe" name="appe" type="text" placeholder="Email..." style="height: 25px; width: 50%;"/>
									</div>
									<div class="formHolder">
										<textarea class="input_form" id="appm" class="tarea" name="appm" rows="5" cols="0" style="width: 50%;" placeholder="Your Message..."></textarea>
									</div>
									<div class="formHolder">
										<input class="input_form" id="appp" name="appp" type="text" placeholder="Phone Number..."  style="height: 25px; width: 50%;"/>
									</div>
									<input class="submit" name="submit" value="Send Message" type="submit" />
								</form>
								<div class="social">
									<a href="https://www.facebook.com/babyboomphotography/"><div class="social_holder" style="background-image: url(imgs/facebook.png);"></div></a>
									<a href="https://www.instagram.com/babyboomphotographer/"><div class="social_holder" style="background-image: url(imgs/instagram.png);"></div></a>
								</div>
								</center>

						</div>

		    </div>
		</div>
		<div class="site-footer"></div>
		 <div class="postFooter">
			 <div id="bottomRepeat"></div>
			</div>
</div>
		<!-- JavaScript at the bottom for fast page loading -->

		<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/jquery-1.7.2.min.js"><\/script>')</script>

	    -->
	    <script src="js/jq.js"></script>
	    <script src="js/slide.js"></script>
	    <script>
	        $(document).ready(function() {
						$('.sent-close').click(function(){
							$('.sent-message').hide();
						});

	            $('.menu-link').bigSlide();
	        });
	    </script>
		<script src="js/backstretch.js"></script>
</body>
</html>
