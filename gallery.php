<?
include($_SERVER['DOCUMENT_ROOT'].'/octopus/includes/config.php');
//Include database connection detail
include($_SERVER['DOCUMENT_ROOT'].'/octopus/includes/functions.php');
// Connect to database
$db = new Database;

$db->query("select * from octo_attachments");
$data = $db->resultset();

$n = count($data);
//Create a new Object
$db = new Database;
//Run a Query
$db->Query("select * from octo_attachments where pid_type = 'custom1'");
//Execute a the Query
$data = $db->resultset();
//Place Rowcount from query into a variable
$TotalItems = $db->rowcount();
//Set Total if rows for pagination



?>

<!DOCTYPE html>
<!-- HTML5 Mobile Boilerplate -->
<!--[if IEMobile 7]><html class="no-js iem7"><![endif]-->
<!--[if (gt IEMobile 7)|!(IEMobile)]><!-->
<html class="no-js" lang="en"><!--<![endif]-->

<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

<head>
	<meta charset="utf-8">
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Baby Boom Photography</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta http-equiv="cleartype" content="on">
	<link rel="shortcut icon" href="/favicon.ico">

	<meta property="og:url" content="http://stingrayspamfilter.com/dev/bbp/index.php"/>
	<meta property="og:type" content="website"/>
	<meta property="og:title" content="Baby Boom Photography"/>
	<meta property="og:description" content="Thank you for visiting my site. How lucky am I to have a job that I love.  I love to capture a magic moment and freeze it in time. Creating a memory that you can keep forever."/>
	<meta property="og:image" content="http://stingrayspamfilter.com/dev/bbp/imgs/og_logo.png"/>

	<!-- Responsive and mobile friendly stuff -->
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/html5reset.css" media="all">
	<link rel="stylesheet" href="css/responsivegridsystem.css" media="all">
  <link rel="stylesheet" href="css/main.css" media="all">
	<link rel="stylesheet" href="css/col.css" media="all">
	<link rel="stylesheet" href="css/2cols.css" media="all">
	<link rel="stylesheet" href="css/3cols.css" media="all">
	<link rel="stylesheet" href="css/4cols.css" media="all">
	<link rel="stylesheet" href="css/5cols.css" media="all">
	<link rel="stylesheet" href="css/6cols.css" media="all">
	<link rel="stylesheet" href="css/7cols.css" media="all">
	<link rel="stylesheet" href="css/8cols.css" media="all">
	<link rel="stylesheet" href="css/9cols.css" media="all">
	<link rel="stylesheet" href="css/10cols.css" media="all">
	<link rel="stylesheet" href="css/11cols.css" media="all">
	<link rel="stylesheet" href="css/12cols.css" media="all">
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/transitions.css" media="all">
	<link rel="stylesheet" type="text/css" href="js/jquery.lightbox.css">
	<link rel="stylesheet" type="text/css" href="css/lightgallery.css">
	<link rel="icon"
			type="image/png"
			href="imgs/Logo.png">
	<!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements and feature detects -->
	<script src="js/modernizr-2.5.3-min.js"></script>

	<style type="text/css">
/*
Recommeded CSS
==============
*/
.lg-outer .lg-img-wrap {
	right: 50%!important;
	left: -50%!important;
}

li.list-group-item.upImhLi {
width: 100%;
float: left;
}

.gallery_images {
height: 270px;
width: 370px;
background-size: cover;
background-position: center;
}

.gallery_images_css{
	margin-left: 12px!important;
}

		.install-gallery {
				padding-bottom: 10px;
		}
		.install-gallery > ul {
			margin-bottom:0;
		}
		.install-gallery > ul > li {
				float: left;
				margin-top: 15px;
				margin-left: 20px;
				/* width: 200px; */
				margin-bottom: 0px;
		}

		.install-gallery > ul > li a {
			border: 3px solid #FFF;
			border-radius: 3px;
			display: block;
			overflow: hidden;
			position: relative;
			float: left;
		}
		.install-gallery > ul > li > a >.gallery_images:hover {
			-webkit-transform: scale(1.2);
			transform: scale(1.2);
			cursor: pointer;
			transition: 0.3s;
		}
</style>
</head>
<body>
 <nav id="mobmenu" class="panel" role="navigation">
      <ul class="mobList">
          <li><a href="index.php">Home</a></li>
          <li><a href="gallery.php">Gallery</a></li>
          <li><a href="prices.php">Prices</a></li>
          <li><a href="cake_smash.php">Cake Smash</a></li>
          <li><a href="newborn_session.php">Newborn Session</a></li>
          <li><a href="blog.php">Blog</a></li>
          <li><a href="contact_us.php">Contact</a></li>
      </ul>
         <a href="#mobmenu" class="sldr-menu-link">Close <i class="fa fa-bars"></i></a>
  </nav>
  <div class="wrap push">
 <div id="rwd_indicator"> <!--TopBar-->
  <div id="topRepeat"></div>
 </div>
 <div class="logo">
  <div class="col span_3_of_3" id="logoBar">
   <!-- This is where the image will go-->
   <a href="index.php"><img src="imgs/Logo.png" alt="Logo"></a>
  </div>
 </div>
 <div id="menu">
 <div id="mobTop">
    <a href="#mobmenu" class="menu-link">Menu <i class="fa fa-bars"></i></a>
    <div id="call">

      </div>
 </div>
   <ul class="mainNav">
       <li><a href="index.php">Home</a></li>
       <li><a href="gallery.php">Gallery</a></li>
       <li><a href="prices.php">Prices</a></li>
       <li><a href="cake_smash.php" style="width:148px;">Cake Smash</a></li>
<li><a href="newborn_session.php" style="">Newborn Session</a></li>
       <li><a href="blog.php">Blog</a></li>
       <li><a href="contact_us.php">Contact</a></li>
   </ul>
 </div>
		<div id="wrapper">
		    <div class="maincontent">
					<div class="section group">
						<div class="col span_3_of_3">
								<h2>Gallery</h2>
						</div>

<div class="install-gallery">
								<ul id="" class="lightgallery list-unstyled row">
									<?
									foreach ($data as $image) {
										$location = "http://babyboomphotography.co.uk/octopus/uploads/".$image['filename'];
										?>
										<li class="col span_1_of_3 gallery_images_css" style="" data-responsive="<?echo $location; ?>" data-src="<?echo $location; ?>">
											<a href="">
													<div class="gallery_images" style="background-image:url('<?echo $location; ?>');">
															<img class="img-responsive" src="<?echo $location;?>" style="display:none;">
													</div>
											</a>
									</li>
										<?
									}
									?>
								</ul>
							</div>
							<div class="section group">
								<br /><br />
							</div>
							<?echo $db->paginate_display_pages();?>
					</div>
		    </div>
		</div>
		<div class="site-footer"></div>
		 <div class="postFooter">
			 <div id="bottomRepeat"></div>
			</div>
</div>
		<script src="js/jq.js"></script>
		<script src="js/slide.js"></script>
		<script src="js/backstretch.js"></script>
		<script src="js/jquery.collagePlus.js"></script>
		<script src="js/jquery.removeWhitespace.js"></script>
    <script src="js/jquery.collageCaption.js"></script>
		<script src="js/jquery.lightbox.js"></script>
		<script src="js/lightgallery.js"></script>
		 <script>
		 $(document).ready(function(){
					$('.lightgallery').lightGallery();
			});
/*	        $(document).ready(function() {
						setTimeout(
  function()
  {

	            $('.menu-link').bigSlide();
							collage();
							$('.Collage').collageCaption();
							//do something special
						}, 1500);
	        });*/

					function collage() {
					 $('.Collage').removeWhitespace().collagePlus(
							{
								'fadeSpeed'     : '3000',
								'targetHeight'  : 200,
						 		'allowPartialLastRow' : false,
								'effect' : 'effect-5'
						 	}
			 			);
	 				};
					var resizeTimer = 1000;
						$(window).bind('resize', function() {
        		// hide all the images until we resize them
        			$('.Collage .Image_Wrapper').css("opacity", 0);
        			// set a timer to re-apply the plugin
        			if (resizeTimer) clearTimeout(resizeTimer);
        			resizeTimer = setTimeout(collage, 500);
    			});

					$(function()
						{
							$('[rel = "lightbox"]').lightbox();
						});

	    </script>


</body>
</html>
