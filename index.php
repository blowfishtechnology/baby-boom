<?
include($_SERVER['DOCUMENT_ROOT'].'/octopus/includes/config.php');
//Include database connection detail
include($_SERVER['DOCUMENT_ROOT'].'/octopus/includes/functions.php');
// Connect to database
$db = new Database;

$db->query("select * from octo_attachments where pid_type = 'custom3' AND  pid = '1'");
$data = $db->resultset();


if(isset($_GET['sent'])){
	$sent = '1';
}else{
	$sent = '0';
}

//var_dump($data);

?>

<!DOCTYPE html>
<!-- HTML5 Mobile Boilerplate -->
<!--[if IEMobile 7]><html class="no-js iem7"><![endif]-->
<!--[if (gt IEMobile 7)|!(IEMobile)]><!-->
<html class="no-js" lang="en"><!--<![endif]-->

<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

<head>
	<meta charset="utf-8">
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Baby Boom Photography</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta http-equiv="cleartype" content="on">
	<link rel="shortcut icon" href="/favicon.ico">

	<meta property="og:url" content="http://stingrayspamfilter.com/dev/bbp/index.php"/>
	<meta property="og:type" content="website"/>
	<meta property="og:title" content="Baby Boom Photography"/>
	<meta property="og:description" content="Thank you for visiting my site. How lucky am I to have a job that I love.  I love to capture a magic moment and freeze it in time. Creating a memory that you can keep forever."/>
	<meta property="og:image" content="http://stingrayspamfilter.com/dev/bbp/imgs/og_logo.png"/>
	<!-- Responsive and mobile friendly stuff -->
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/html5reset.css" media="all">
	<link rel="stylesheet" href="css/responsivegridsystem.css" media="all">
  <link rel="stylesheet" href="css/main.css" media="all">
	<link rel="stylesheet" href="css/col.css" media="all">
	<link rel="stylesheet" href="css/2cols.css" media="all">
	<link rel="stylesheet" href="css/3cols.css" media="all">
	<link rel="stylesheet" href="css/4cols.css" media="all">
	<link rel="stylesheet" href="css/5cols.css" media="all">
	<link rel="stylesheet" href="css/6cols.css" media="all">
	<link rel="stylesheet" href="css/7cols.css" media="all">
	<link rel="stylesheet" href="css/8cols.css" media="all">
	<link rel="stylesheet" href="css/9cols.css" media="all">
	<link rel="stylesheet" href="css/10cols.css" media="all">
	<link rel="stylesheet" href="css/11cols.css" media="all">
	<link rel="stylesheet" href="css/12cols.css" media="all">
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Amatic+SC|Fredericka+the+Great" rel="stylesheet">
	<link rel="icon"
      type="image/png"
      href="imgs/Logo.png">

	<!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements and feature detects -->
	<script src="js/modernizr-2.5.3-min.js"></script>

	<!-- font-family: 'Amatic SC', cursive; -->
	<!-- font-family: 'Fredericka the Great', cursive; -->
	<style type="text/css">
	p{
		    line-height: 22px;
	}

.sent-message{
	z-index: 1000000;
	position: absolute;

	height: 170px;
	width: 400px;
	top: 50%;
	left: 50%;
	margin-left: -200px;
	-webkit-box-shadow: 10px 10px 37px -6px rgba(0,0,0,0.75);
	-moz-box-shadow: 10px 10px 37px -6px rgba(0,0,0,0.75);
	box-shadow: 10px 10px 37px -6px rgba(0,0,0,0.75);
	border-top: 2px solid #C0B3AB;
	border-bottom: 2px solid #C0B3AB;
}

.sent-message-inner{
	background-color: white;
	height: 100%;
	width: 100%;
}

.sent-text{
	font-family: 'Amatic SC', cursive;
	font-size: 22px;
}

.sent-close{
	cursor: pointer;
}

.welcomeP{
	line-height: 120%;
}

@media only screen and (max-width: 480px) {
	.sent-message{
		height: 170px;
		width: 300px;
		margin-left: -150px;
	}
}

/*
Recommeded CSS
==============
*/
</style>
</head>
<body>
 		<nav id="mobmenu" class="panel" role="navigation">
        <ul class="mobList">
            <li><a href="index.php">Home</a></li>
            <li><a href="gallery.php">Gallery</a></li>
            <li><a href="prices.php">Prices</a></li>
												<li><a href="cake_smash.php">Cake Smash</a></li>
												<li><a href="newborn_session.php">Newborn Session</a></li>
            <li><a href="blog.php">Blog</a></li>
            <li><a href="contact_us.php">Contact</a></li>
        </ul>
          	<a href="#mobmenu" class="sldr-menu-link">Close <i class="fa fa-bars"></i></a>
    </nav>
    <div class="wrap push">
			<div id="rwd_indicator"> <!--TopBar-->
				<div id="topRepeat"></div>
			</div>
			<div class="logo">
				<div class="col span_3_of_3" id="logoBar">
					<!-- This is where the image will go-->
					<a href="index.php"><img src="imgs/Logo.png" alt="Logo"></a>
				</div>
			</div>
			<div id="menu">
			<div id="mobTop">
	  			<a href="#mobmenu" class="menu-link">Menu <i class="fa fa-bars"></i></a>
						<div id="call">

	    			</div>
			</div>
					<ul class="mainNav">
					    <li><a href="index.php">Home</a></li>
					    <li><a href="gallery.php">Gallery</a></li>
					    <li><a href="prices.php">Prices</a></li>
									<li><a href="cake_smash.php" style="width:148px;">Cake Smash</a></li>
<li><a href="newborn_session.php" style="">Newborn Session</a></li>
					    <li><a href="blog.php">Blog</a></li>
					    <li><a href="contact_us.php">Contact</a></li>
					</ul>
			</div>
			<?if($sent == 1){?>
			<div class="sent-message">
				<div class="sent-message-inner">
					<p class="sent-text" style="padding-top: 10px;">
						Thank you for filling out the resquest callback form.
					</p>
					<p class="sent-text">
						I will get back to you as soon as possible.
					</p>
					<i class="fa fa-times sent-close"></i>
				</div>
			</div>
			<?}?>
		<div id="wrapper">
		    <div class="maincontent">

					<div class="upperContent">
							<div class="col span_3_of_9">
								<div class="welcomeOuter">
									<h2 class="welcomeH2">
										Welcome
									</h2>
									<div class="welcomeInner">
										<p class="welcomeP">
											Thank you for visiting my site. How lucky am I to have a job that I love. Not only do I have my own studio but for the last 7 years I have had the pleasure
											of working in a large Liverpool hospital taking photographs of new born babies. I am a mum of 6 grown up children and a nan to 4 grandchildren.
											My work background is in graphic design and photography. I love to capture a magic moment and freeze it in time. Creating a memory that you can keep forever.
										</p>
									</div>
								</div>
							</div>
							<div class="col span_1_of_9">
							</div>
							<div class="col span_5_of_9">
								<div class="homeImage">
									<?
										foreach ($data as $image) {
											$location = "http://babyboomphotography.co.uk/octopus/uploads/".$image['filename'];
											?>
											<img class="mySlides" src="<?echo $location;?>"/>
											<?
										}
									?>
									<div class="btnHolder">
										<div class="btn-padding-left">
											<a class="btn-left btn-floating btn-hover-text" onclick="plusDivs(-1)">&#10094;</a>
										</div>
										<div class="btn-padding-right">
											<a class="btn-right btn-floating btn-hover-text" onclick="plusDivs(1)">&#10095;</a>
										</div>

									</div>
								</div>
							</div>
					</div>
							<div class="homePageGap">
							 <div class="col span_3_of_3">

							 </div>
						 </div>
						 <div class="section group">
							 <div class="col span_3_of_3">
								 <strong>About Me</strong>
								 <div class="clear-fix"></div>
								 <p>
										Hi I am Caroline McNally
										I am a  professional photographer and Graphic Designer with over 15 years experience.
										Specializing in Newborn and Family portraiture. I have a home studio  in Ormskirk Lancashire.
									</p>
								 <strong>At the Studio</strong>
								 <div class="clear-fix"></div>
								 <p>
										Normaly coming along to my studio will be one of your first days out,
										so it is important for me to take care of new mums too.
										I make the time you spend in my studio relaxing and enjoyable.

										The best thing about newborns for me is their tiny characters
										and as much as I love to pose and create the portrait,  newborns have their  own plans.
										This is what I love about my job. because every day is diffrent.
										Some newborns dont like going on their belly, some love to stay wide awake ,
										some dont like to be wrapped  and some spread out totally flat
										like they have just been on the best night out ever.
										But to me these poses are what makes the photoshot unique  resulting in the best portraites.
									</p>
							 </div>
						 </div>
					 	<div class="section group">
		          <div class="col span_3_of_3">
		            <h2 id="homePageH2">
									Captured moments make amazing memories
								</h2>
		          </div>
		        </div>
		    </div>
		</div>
		<div class="site-footer"></div>
		 <div class="postFooter">
			 <div id="bottomRepeat"></div>
			</div>
</div>
		<!-- JavaScript at the bottom for fast page loading -->

		<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/jquery-1.7.2.min.js"><\/script>')</script>

	    -->
	    <script src="js/jq.js"></script>
	    <script src="js/slide.js"></script>
	    <script>
	        $(document).ready(function() {
	            $('.menu-link').bigSlide();

							$('.sent-close').click(function(){
								$('.sent-message').hide();
							});
	        });

					var myIndex = 0;
					carousel();

					var slideIndex = 1;
					showDivs(slideIndex);

					function plusDivs(n) {
					  showDivs(slideIndex += n);
					}

					function showDivs(n) {
					  var i;
					  var x = document.getElementsByClassName("mySlides");
					  if (n > x.length) {slideIndex = 1}
					  if (n < 1) {slideIndex = x.length}
					  for (i = 0; i < x.length; i++) {
					     x[i].style.display = "none";
					  }
					  x[slideIndex-1].style.display = "block";
					}

					function carousel(){
						var i;
						var x = document.getElementsByClassName("mySlides");
 						for (i = 0; i < x.length; i++) {
								x[i].style.display = "none";
						 }
						 myIndex++;
						 if (myIndex > x.length) {myIndex = 1}
						 x[myIndex-1].style.display = "block";
						 setTimeout(carousel, 10000); // Change image every 10 seconds
					}
	    </script>
		<script src="js/backstretch.js"></script>
</body>
</html>
