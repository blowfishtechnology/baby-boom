<?
require '../app_php/class.phpmailer.php';
$name = $_POST['appn'];
$message = $_POST['appm'];
$phone = $_POST['appp'];
$email = $_POST['appe'];

if($name == "" && $phone = "" || $email ==""){
  header('Location: ../contact_us.php?blank');
}else{
  $emailBody = '
  <html>
  <head>
  <title>HTML email</title>
  </head>
  <body>
  <div class="master" style="  width:100%; min-height: 200px; background-color: #f3eeea; border-top: 1px solid #65594a;border-bottom: 1px solid #65594a;">
  <div class="logo" style="display:table;margin: 0 auto;">
  </div>
  <div class="content" style="display:table; margin: 0 auto;">
  <p> A callback has been requested from Baby Boom Photography </p>
    <ul>
      <li style="list-style:none;padding:10px;margin-top:10px;margin-bottom:10px;background-color: rgba(192,179,171,0.1);"><strong>Name:</strong> '.$name.' </li>
      <li style="list-style:none;padding:10px;margin-top:10px;margin-bottom:10px;background-color: rgba(192,179,171,0.1);"><strong>Email:</strong> '.$email.'  </li>
      <li style="list-style:none;padding:10px;margin-top:10px;margin-bottom:10px;background-color: rgba(192,179,171,0.1);"><strong>Phone:</strong> '.$phone.'  </li>
      <li style="list-style:none;padding:10px;margin-top:10px;margin-bottom:10px;background-color: rgba(192,179,171,0.1);"><strong>Message:</strong> '.$message.' </li>
    </ul>
  </div>
  </div>
  </body>
  </html>';

  //Create a new PHPMailer instance
  $mail = new PHPMailer;
  //Set who the message is to be sent from
  $mail->setFrom('admin@babyboomphotography.co.uk', 'Baby Boom Photography');//Spoof the website
  //Set who the message is to be sent to
  $mail->addAddress('jmac199528@gmail.com');//This will be to baby boom
  //Set the subject line
  $mail->Subject = 'Baby Boom Photography Request Callback';
  //Read an HTML message body from an external file, convert referenced images to embedded,
  //convert HTML into a basic plain-text alternative body
  $mail->msgHTML($emailBody);
  //Replace the plain text body with one created manually
  $mail->AltBody = 'This is a plain-text message body';
  //Attach an image file

  //send the message, check for errors
  if (!$mail->send()) {
      echo "Mailer Error: " . $mail->ErrorInfo;
      header('Location: ../contact_us.php?error');
  } else {
      echo "Message sent!";
      header('Location: ../index.php?sent');
  }
}
